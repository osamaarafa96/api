@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="table-responsive">


                    @if(isset($users) && !empty($users) && count($users) != 0)
                        <table id="Example1" class="table table-bordered table-striped table-hover">
                            <thead>
                               <tr class="info">
                                  <th>num</th>
                                  <th>name </th>
                                  <th>E-mail</th>
                                  <th>process</th>
                               </tr>
    
                            </thead>
                            <tbody>
    
    
                                  <?php $x=1; ?>
                                    @foreach($users as $user)
                                   <tr>
                                      <td>{{$x}} </td>
                                      <td>{{$user->name}}</td>
                                      <td>{{$user->email}}</td>
                                      <td>
                                        <a href="{{asset('company/'.$user->id).'/edit'}}" class="btn btn-success btn-sm">
                                            Edit</a>
                                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#delete{{$user->id}}">Delete </button>
                                      </td>
    
                                   </tr>
                                   <?php $x++; ?>
    
                               @endforeach
    
    
                            </tbody>
                         </table>
                         @else(!isset($users) && empty($users) count($users) == 0)
                        <div class="alert alert-danger fade in">
                                Sorry
                            </div>
                          @endif
                      </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                   
@foreach($users as $user)
<!-- delete user Modal2 -->
<div class="modal fade" id="delete{{$user->id}}" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
     <div class="modal-content">
        <div class="modal-header modal-header-primary">
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
           <h3><i class="fa fa-building m-r-5"></i>  </h3>
        </div>
        <div class="modal-body">
           <div class="row">
              <div class="col-md-12">
                 <form method="post" action="{{asset('user/'.$user->id)}}" class="form-horizontal">
                    <fieldset>
                       <div class="col-md-12 form-group user-form-group">
                          <label class="control-label">Confirm Deleting</label>
                          <div class="pull-right">
                            {{csrf_field()}}
                            <input name='_method' value='DELETE' type="hidden" />
                             <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">No</button>
                             <button type="submit" class="btn btn-success btn-sm">Yes</button>
                          </div>
                       </div>
                    </fieldset>
                 </form>
              </div>
           </div>
        </div>
        <div class="modal-footer">
           <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" >close</button>
        </div>
     </div>
     <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
